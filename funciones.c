/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdio.h>           //importo las librerias y los headers
#include "funciones.h"       //

#define TRUE 1               // defino true como 1
#define FALSE 0              // defino false como 0

port_16_t port_d;            //definimos el tipo de dato estructura del puerto d

int bitset (char puerto,unsigned int bit)          //es una función que dado un puerto y un numero de bit cambia su estado a 1
{
    int flag_error=0;                              //iniciamos una variable de error con 0
    
    if (((puerto==PORT_A)||(puerto==PORT_B)) && ((0<=bit)&&(bit<=7)))  //sí es el puerto A o B y el bit esta entre 0 y 7
    {
    {
        if (puerto==PORT_A)
        {
            port_d.port_a=port_d.port_a | (1<<bit);    //  asignamos la posición del bit un 1
        }
        else
        {
            port_d.port_b= port_d.port_b | (1<<bit);   //  accedo al bit del puerto b y
        }
        }    
    }
    else if ((puerto==PORT_D) && ((0<=bit)&&(bit<=7))) //sí es el puerto D y el bit esta entre 0 y 7
    {
        port_d.port_b= port_d.port_b | (1<<bit);         // asignamos a la posición del bit un 1
    }
    else if ((puerto==PORT_D) && ((8<=bit)&&(bit<=15))) //si es el puerto D y el bit esta entre 8 y 15
    {
        port_d.port_a= port_d.port_a | (1<<(bit-8));   // asignamos a la posicion del bit un 1 
    }                                                  
    else                                               
    {
        printf("error bitSet\n");
        flag_error=ERROR;                               //asignamos a la variable error, el error
    }
    return flag_error;                                  //devuelve la variable error tipo int como 0 o 1, siendo 0 no error y 1 error
}

int bitclr (char puerto,unsigned int bit)     //es una función que dado un puerto y un número de bit, cambia su estado a 0
{
    int flag_error=0;                         // de forma analoga a la función bitset
    
    if (((puerto==PORT_A)||(puerto==PORT_B)) && ((0<=bit)&&(bit<=7)))      // de forma analoga a la función bitset
    {
        if (puerto==PORT_A)
        {
            port_d.port_a=port_d.port_a & ~(1<<bit);                        //hace una limpieza de lo que haya en el bit del puerto a
        }
        else
        {
            port_d.port_b= port_d.port_b & ~(1<<bit);                      //hace una limpieza de lo que haya en el bit del puerto b
        }    
    }
    else if ((puerto==PORT_D) && ((0<=bit)&&(bit<=7)))                     // de forma analoga a la función bitset
    {
        port_d.port_b= port_d.port_b & ~(1<<bit);                          //hace una limpieza de lo que haya en el bit seleccionado del puerto b
    }
    else if ((puerto==PORT_D) && ((8<=bit)&&(bit<=15)))                   // de forma analoga a la función bitset
    {
        port_d.port_a= port_d.port_a & ~(1<<(bit-8));                     //hace una limpieza de lo que haya en el bit seleccionado del puerto a
    }
    else                                                                  // de forma analoga a la función bitset
    {
        printf("error bitClr\n");
        flag_error=ERROR;
    }
    return flag_error;                                                    // de forma analoga a la función bitset
}
int getbit (char puerto, unsigned int bit)         //función que dado un puerto y el numero de valor, devuelve su valor
{
    int vuelta=0;
    
    if (((puerto==PORT_A)||(puerto==PORT_B)) && ((0<=bit)&&(bit<=7)))     //de forma analoga al bitset
    {
        if (puerto==PORT_A)
        {
            vuelta= (port_d.port_a>>bit)&1;                               // vuelta es igual a lo que hay en el puerto a en la posicion del bit
        }
        else
        {
            vuelta= (port_d.port_b>>bit)&1;                              // vuelta es igual a lo que hay en el puerto b en la posicion del bit      
        }    
    }
    else if ((puerto==PORT_D) && ((0<=bit)&&(bit<=7)))                    //se forma analoga al bitse  
    {
        vuelta= (port_d.port_b>>bit)&1;                                   // vuelta es igual a lo que hay en el puerto d en la posicion del bit de 0 a 7
    }
    else if ((puerto==PORT_D) && ((8<=bit)&&(bit<=15)))
    {
        vuelta= (port_d.port_a>>(bit-8))&1;                               // vuelta es igual a lo que hay en el puerto a en la posicion del bit 8 al 15
    }
    else
    {
        printf("error getbit\n");                                         //analogo a la forma del bitset
        vuelta=ERROR;
    }
    return vuelta;
}
int bittoggle (char puerto,unsigned int bit)        //funcion que dado un puerto y un numero de bit cambia el estado al opuesto en el que esta
{
    int flag_error=0;
    
    if (((puerto==PORT_A)||(puerto==PORT_B)) && ((0<=bit)&&(bit<=7)))   //analogo a bitset
    {
        if (puerto==PORT_A)
        {
            port_d.port_a=port_d.port_a ^ (1<<bit);                    //invierte lo que haya en la posición del bit
        }
        else
        {
            port_d.port_b= port_d.port_b ^ (1<<bit);                   //invierte lo que haya en la posición del bit
        }    
    }
    else if ((puerto==PORT_D) && ((0<=bit)&&(bit<=7)))                 //analogo al bitset
    {
        port_d.port_b= port_d.port_b ^ (1<<bit);                       //invierte lo que haya en la posición del bit
    }
    else if ((puerto==PORT_D) && ((8<=bit)&&(bit<=15)))                //analogo al biteset
    {
        port_d.port_a= port_d.port_a ^ (1<<(bit-8));                    //invierte lo que haya en la posición del bit
    }
    else
    {
        printf("error bitToggle\n");                                    //analogo al bitset
        flag_error=ERROR;
    }
    return flag_error;
}
int maskon(char puerto,unsigned int mask)                             //dado un puerto y una mascara , debe prender aquellos bits que esten prendidos en la mascara sin cambiar el estado de los restantes
{
    int flag_error=0;
    
    if (((puerto==PORT_A)||(puerto==PORT_B)) && ((0<=mask)&&(mask<=255)))        //sí el puerto es el a o b y la mask va de 0 a 255
    {
        if (puerto==PORT_A)
        {
            port_d.port_a|=mask ;                                               //lo contenido en el puerto a más la mascara
        }
        else
        {
            port_d.port_b|= mask;                                               //lo contenido en el puerto b más la mascara
        }    
    }
    else if ((puerto==PORT_D)&&(0<=mask && mask<=0xFFFF))                     //sí el puerto es el d y la mask va de 0 a 0xFFFF, ejecuta
    {
        port_d.port_b|=(mask&0x00FF);                                         //en el byte del puerto b, el menos significativo aplicamos la mascara con una AND con 0x00FF
        port_d.port_a|=((mask&0xFF00)>>8);                                    //en el byte del puerto a, el mas significativo aplicamos la mascara con una AND con 0xFF00
    }
    else
    {
        printf("error maskon\n");                                           //hay un  error en las entradas
        flag_error=ERROR;
    }
    return flag_error;
}
int maskoff(char puerto,unsigned int mask)       //dado un puerto y una mascara , debe apagar todos los bits que esten contenidos en la mascara sin cambiar los restantes
{
    int flag_error=0;
    
    if (((puerto==PORT_A)||(puerto==PORT_B)) && ((0<=mask)&&(mask<=255)))        //la condición es analoga a la de maskon
    {
        if (puerto==PORT_A)
        {
            port_d.port_a&=(~mask) ;                                             // a lo contenido en el puerto a aplica un AND logico con la negación de la mascara
        }
        else
        {
            port_d.port_b&=(~mask);                                              // a lo contenido en el puerto b aplica un AND logico con la negación de la mascara
        }    
    }
    else if ((puerto==PORT_D)&&(0<=mask && mask<=0xFFFF))                        //analogo a la funcion maskon
    {
        port_d.port_b&=((~mask)&0x00FF);                                         //a lo contenido en el puerto b aplica una mascara de la negada de la mascara con una AND de 0x00FF (byte menos significativo)
        port_d.port_a&=(((~mask)&0xFF00)>>8);                                    //a lo contenido en el puerto a aplica una mascara de la negada de la mascara con una AND de 0xFF00 (byte mas significativo)
    }
    else
    {
        printf("error maskoff\n");                                               //analogo a la función maskon
        flag_error=ERROR;
    }
    return flag_error;
}
int masktoggle(char puerto,unsigned int mask)       //  dado un puerto y una mascara, debe cambiar el estado de todos aquellos bits que esten prendidos en la mascara del opuesto
{
    int flag_error=0;
    
    if (((puerto==PORT_A)||(puerto==PORT_B)) && ((0<=mask)&&(mask<=255)))        //analogo a la funcion maskon
    {
        if (puerto==PORT_A) 
        {
            port_d.port_a^=mask ;                                                //a lo contenido en el puerto a le aplico un XOR con la mascara
        }
        else
        {
            port_d.port_b^= mask;                                                 //a lo contenido en el puerto b le aplico un XOR con la mascara
        }    
    }
    else if ((puerto==PORT_D)&&(0<=mask && mask<=0xFFFF))                         //analogo a la función maskon
    {
        port_d.port_b^=(mask&0x00FF);                                             //a lo contenido en el puerto b le hago un XOR con la AND de la mascara y 0x00FF (byte menos significativo)
        port_d.port_a^=((mask&0xFF00)>>8);                                        //a lo contenido en el puerto a le hago una XOR con la AND de la mascara y 0xFF00 (byte más significativo)
    }
    else
    {
        printf("error masktoggle\n");                                             //analogo a la función maskon
        flag_error=ERROR;
    }
    return flag_error;
}
