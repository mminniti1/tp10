/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: matias
 *
 * Created on May 17, 2023, 12:01 PM
 */

#include <stdio.h>     //incluyo las librerias y los header del programa
#include <stdlib.h>
#include "funciones.h"
#include "teclado.h"   //

/*
 * 
 */
int main(int argc, char** argv) 
{
    char input=0;           
    int flag_salida=1;   
    while(flag_salida)         //entra en bucle si la variable flag esta en 1   
    {
        input=teclado();
        if(('0'<=input)&&(input<='7'))       //si el numero de input es 0<= y es <=7 ejecuta la funcion biset 
        {
            bitset(PORT_A,(input-'0'));
        }
        else      //en caso contrario estamos en presencia de letras
        {
            switch (input)  //analiza cada caso particular
            {
                case 't':
                    masktoggle(PORT_A,0xFF);    //sí es t, inicializa la funcion masktoggle
                    break;
                case 's':
                    maskon(PORT_A,0xFF);        //sí es s, inicializa la funcion maskon 
                    break;
                case 'c':
                    maskoff(PORT_A,0xFF);       //sí es c, inicializa la funcion maskoff
                    break;
                case 'q':                       //sí la letra es q, salimos del bucle
                    flag_salida=0;
                    break;
                default:                                  //sí no se presiona ninguna  lanza un mensaje de error por consola
                    printf("no es un comando valido\n");
                    break;
            }
        }
        printf("\nlos leds prendidos estan representados con un 1 y con 0 los apagados\n");
        for (int i=0; i<8; i++)                    // es un for de getbit 
        {
            printf("|%d",getbit(PORT_A,(7-i)));
        }
        printf("|\n");
    } 
    return (EXIT_SUCCESS);
}

