/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdio.h>
char teclado (void)            //la función ejecuta prints para la lectura del usuario y luego espera que presione una letra de las opciones para almacenarla
{
    char input;                                       
    printf("Ingrese uno de los siguientes comandos:\n");
    printf("   -Numero del 0 al 7, para prender un led\n");
    printf("   -t para cambiar el estado de todos los leds al opuesto\n");
    printf("   -c para apagar todos los leds\n");
    printf("   -s para prender todos los leds\n");
    printf("   -q para terminar el programa\n");
    input=getchar();
    while (getchar() != '\n')   //si el getchat es distinto del enter inializa el bloque
    {
        input='w';
    }
    return input;               //devuelve una variable tipo char donde almacena la entrada
}