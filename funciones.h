/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   funciones.h
 * Author: facundo
 *
 * Created on 19 de mayo de 2023, 18:05
 */

#ifndef FUNCIONES_H    //defino el header funciones con los prototipos y macros
#define FUNCIONES_H

#include <stdint.h>

#define PORT_A 1     //defino los puertos
#define PORT_B 2
#define PORT_D 3
#define ERROR -1     //

typedef struct 
{
    uint8_t port_b;      //definimos una estructura tomando portb como el menos significativo 
    uint8_t port_a;      //definimos port a como el más significativo y la agrupacion de ambos nos forma la estructura port_d
}port_16_t;           

extern port_16_t port_d;    //exportamos la estructura de port_d para el archivo fuente funciones

int bitset(char,unsigned int);          //defino los protipos de las funciones operacionales 
int bitclr(char,unsigned int);          
int getbit(char,unsigned int);
int bittoggle(char,unsigned int);
int maskon(char,unsigned int);
int maskoff(char,unsigned int);
int masktoggle(char,unsigned int);      //

#endif /* FUNCIONES_H */


